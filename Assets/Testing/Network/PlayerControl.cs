﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerControl : NetworkBehaviour {

    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    void Update()
    {
        if (!isLocalPlayer)
            return;

        // Move player
        //var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        // var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;

        //transform.Rotate(0, x, 0);
        //transform.Translate(0, 0, z);

        CharacterController cc = GetComponent<CharacterController>();
        Vector3 pos = transform.position;

        Vector3 targetPos = transform.position + (new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f) * 5.0f);

        
        Vector3 moveDirection = targetPos - transform.position;
        moveDirection *= Time.deltaTime;

        if (moveDirection != Vector3.zero)
            transform.rotation = Quaternion.LookRotation(moveDirection);
        //if (moveDirection != Vector3.zero)
        //    transform.rotation = Quaternion.LookRotation(moveDirection);
        cc.Move(moveDirection);

        // Fire bullet
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CmdFire();
        }
    }

    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            bulletPrefab,
            bulletSpawn.position,
            bulletSpawn.rotation);

        // Add velocity to the bullet
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6;

        NetworkServer.Spawn(bullet);
        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<MeshRenderer>().material.color = Color.blue;
        transform.eulerAngles = new Vector3(0, 90.0f, 90.0f);
        transform.position = new Vector3(0, 0, 1);
    }
}
