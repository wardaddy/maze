﻿using UnityEngine;
using System.Collections;

public class FollowCharacter : MonoBehaviour {

    public Transform targetObj;
    public float smoothness = 10.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 targetPos = new Vector3(targetObj.position.x, targetObj.position.y, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * smoothness);
        

	}
}
