﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

/// <summary>
/// This class maintains clan data
/// </summary>
public class ClanManager : NetworkBehaviour {

    private static ClanManager m_instance;
    private List<GameObject> m_ctList   = new List<GameObject>();
    private List<GameObject> m_tList    = new List<GameObject>();
    private bool m_hasSelectedTeam     = false;

    public GameObject       m_playerPrefab;
    public List<Transform>  m_CTSpawnPoints;
    public List<Transform>  m_TSpawnPoints;
    public GameObject       m_canvas;
    public bool             m_shouldSpawnTerrorist = false;
    public DerivedNetworManager m_networkManager;
    private string m_localPlayerName;
    private GameObject m_localPlayer = null;

    public void setLocalPlayer(GameObject obj)
    {
        m_localPlayer = obj;
    }

    public GameObject getLocalPlayer()
    {
        return m_localPlayer;
    }

    public void setLocalPlayerName(string name)
    {
        m_localPlayerName = name;
    }

    public string getLocalPlayerName()
    {
        return m_localPlayerName;
    }

    public static ClanManager getInstance() {
        return m_instance;
    }

    void Awake() {
        m_instance = this;
    }

    private void OnEnable()
    {
        CustomDebug.Log("Enabled Clan Manager");
        if(StateManager.getInstance() != null)
            StateManager.getInstance().changeState(StateManager.State.STATE_TEAM_SELECTION);
    }

    public void addCt(GameObject obj) {
        m_ctList.Add(obj);
    }

    public void removeCt(GameObject obj) {
        m_ctList.Remove(obj);
    }

    public List<GameObject> getCtList() {
        return m_ctList;
    }

    public void addT(GameObject obj) {
        m_tList.Add(obj);
    }

    public void removeT(GameObject obj)
    {
        m_tList.Remove(obj);
    }

    public List<GameObject> getTList() {
        return m_tList;
    }

    public int getCtDeathCount() {
        int count = 0;
        foreach (GameObject ct in m_ctList)
        {
            count += ct.GetComponent<PlayerController>().getDeathCount();
            CustomDebug.Log("CT death count : " + count);
        }
        return count;
    }

    public int getTDeathCount() {
        int count = 0;
        foreach (GameObject t in m_tList)
        {
            count += t.GetComponent<PlayerController>().getDeathCount();
            CustomDebug.Log("T death count : " + count);
        }
        return count;
    }
    /// <summary>
    /// This function gets invoked when CounterTerrorist is spawned
    /// </summary>
    /// <param name="obj"></param>
    public void counterTHasSpawned(GameObject obj) {
        
        CustomDebug.Log("CT HAS SPAWNED");
        m_ctList.Add(obj);

        if (CustomDebug.isDebugBuild())
        {
            foreach (GameObject o in m_ctList)
            {
                CustomDebug.Log(o.name);
            }
        }
    }

    /// <summary>
    /// This function gets invoked when CounterTerrorist is destroyed
    /// </summary>
    /// <param name="obj"></param>
    public void counterTHasDestroyed(GameObject obj) {
        
        CustomDebug.Log("CT HAS DESTROYED");
        m_ctList.Remove(obj);

        if ((m_ctList != null) && (m_ctList.Count > 0) && CustomDebug.isDebugBuild())
        {
            foreach (GameObject o in m_ctList)
            {
                CustomDebug.Log(o.name);
            }
        }
    }

    /// <summary>
    /// On Click event
    /// </summary>
    public void spawnCT() {
        CustomDebug.Log("CHOOSE CT");
        
        m_shouldSpawnTerrorist = false;
        //m_canvas.SetActive(false);
        ClientScene.AddPlayer(0);
        StateManager.getInstance().changeState(StateManager.State.STATE_WAITING_FOR_PLAYER);
    }

    /// <summary>
    /// On Click event
    /// </summary>
    public void spawnT() {
        m_shouldSpawnTerrorist = true;
        //m_canvas.SetActive(false);
        ClientScene.AddPlayer(0);
        StateManager.getInstance().changeState(StateManager.State.STATE_WAITING_FOR_PLAYER);
    }

    /// <summary>
    /// callback for joining the game
    /// </summary>
    public void onPlayerJoinGame() {
        //if(isLocalPlayer)
        //    StateManager.getInstance().changeState(StateManager.State.STATE_TEAM_SELECTION);
    }

    /// <summary>
    /// This is called when player is leaves the game
    /// </summary>
    public void onPlayerLeftGame() {
        resetClan();
        m_ctList.Clear();
        m_tList.Clear();
        m_hasSelectedTeam = false;
        m_shouldSpawnTerrorist = false;
        LayerCache.clear();
        StateManager.getInstance().changeState(StateManager.State.STATE_NONE);
    }

    /// <summary>
    /// gets called when terrorist is spawned
    /// </summary>
    /// <param name="obj"></param>
    public void terroristHasSpawned(GameObject obj)
    {
        CustomDebug.Log("T HAS SPAWNED");
        m_tList.Add(obj);

        if (CustomDebug.isDebugBuild())
        {
            foreach (GameObject o in m_tList)
            {
                CustomDebug.Log(o.name);
            }
        }
    }

    /// <summary>
    /// gets called when terrorist is destroyed
    /// </summary>
    /// <param name="obj"></param>
    public void terroristHasDestroyed(GameObject obj)
    {
        CustomDebug.Log("T HAS DESTROYED");  
        m_tList.Remove(obj);

        if ((m_tList != null) && (m_tList.Count > 0) && CustomDebug.isDebugBuild())
        {
            foreach (GameObject o in m_tList)
            {
                CustomDebug.Log(o.name);
            }
        }
    }

    public void createLANServer() {
        m_networkManager.StartHost();    
    }

    public void joinLANServer() {
        m_networkManager.StartClient();
    }

    public void resetClan() {
        NetworkDataManager.getInstance().removeNetworkObjects();

        // Reset Safehouse
        List<GameObject> safeHouseCache = LayerCache.getObjectListInLayer(LayerMask.NameToLayer("safehouse"));
        if (safeHouseCache != null)
        {
            foreach(GameObject safeHouse in safeHouseCache)
            {
                safeHouse.GetComponent<Safehouse>().resetSafehouse();
            }
        }

        // reset Gameplay state
        GameplayManager.getInstance().reset();
    }

    public override void OnNetworkDestroy()
    {
        CustomDebug.Log("on Network destroy");
        resetClan();
    }
}
