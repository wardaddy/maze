﻿using UnityEngine;
using System.Collections;

public class GameplayManager : MonoBehaviour {

    public enum GameplayType {
        TYPE_1,
    }
    public GameplayType m_gameplayType = GameplayType.TYPE_1;

    public enum ResultTeam {
        TEAM_NONE,
        TEAM_CT,
        TEAM_T,
        TEAM_DRAW,
    }
    public ResultTeam m_winningTeam = ResultTeam.TEAM_NONE;
    public ResultTeam m_destroyedSafehouse = ResultTeam.TEAM_NONE;

    private static GameplayManager m_instance = null;

    public static GameplayManager getInstance() {
        return m_instance;
    }

    private void Awake()
    {
        m_instance = this;
    }

    public void setMatchResult(ResultTeam result) {
        m_winningTeam = result;
        CustomDebug.Log("Winning team : " + m_winningTeam);
    }

    public void setSafeDestroyedOf(ResultTeam team) {
        m_destroyedSafehouse = team;
        CustomDebug.Log("Safehouse destroyed of : " + team);
    }

    public void reset() {
        m_winningTeam = ResultTeam.TEAM_NONE;
        m_destroyedSafehouse = ResultTeam.TEAM_NONE;
    }
}
