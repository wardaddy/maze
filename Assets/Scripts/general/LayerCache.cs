﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LayerCache : MonoBehaviour {

    private static Dictionary<int, List<GameObject>> m_layerCache = new Dictionary<int, List<GameObject>>();

    public static void Awake(GameObject gameObject)
    {
        List<GameObject> cache;

        if (!m_layerCache.TryGetValue(gameObject.layer, out cache))
        {
            cache = new List<GameObject>();
            m_layerCache.Add(gameObject.layer, cache);
        }
        CustomDebug.Log("Added object : " + LayerMask.LayerToName(gameObject.layer));
        cache.Add(gameObject);
    }

    public static void changeLayerTo(GameObject gameObject, int layer)
    {
        m_layerCache[gameObject.layer].Remove(gameObject);

        gameObject.layer = layer;
        List<GameObject> cache;
        if (!m_layerCache.TryGetValue(gameObject.layer, out cache))
        {
            cache = new List<GameObject>();
            m_layerCache.Add(gameObject.layer, cache);
        }

        cache.Add(gameObject);
    }

    public static void OnDestroy(GameObject gameObject)
    {
        List<GameObject> cache;
        if (m_layerCache.TryGetValue(gameObject.layer, out cache))
        {
            if (cache.Contains(gameObject))
                cache.Remove(gameObject);
        }
    }

    public static List<GameObject> getObjectListInLayer(int layer)
    {
        List<GameObject> cache = null;
        m_layerCache.TryGetValue(layer, out cache);
        return cache;
    }

    public static void clear()
    {
        m_layerCache.Clear();
    }
}