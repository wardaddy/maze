﻿using UnityEngine;
using System.Collections;

public class StateManager : MonoBehaviour {

    private static StateManager m_instance;

    public static StateManager getInstance() {
        return m_instance;
    }

    public enum State {
        STATE_NONE,
        /*STATE_SERVER,*/ // TODO: Fix button click exception 
        STATE_TEAM_SELECTION,
        STATE_RESULT,
        STATE_PAUSE,
        STATE_GAMEPLAY,
        STATE_WAITING_FOR_PLAYER,
        STATE_WAITING_FOR_CONNECTION,
        };

    private State m_gameState = State.STATE_NONE;

    // State Array needs to be in order of State Enum
    public GameObject[] m_stateArray;

    private void Awake()
    {
        m_instance = this;
    }
    
	// Use this for initialization
	void Start () {
        changeState(State.STATE_NONE);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void changeState(State state) {
        m_stateArray[(int)m_gameState].SetActive(false);
        CustomDebug.Log("deactivate : " + m_stateArray[(int)m_gameState].name);
        m_stateArray[(int)state].SetActive(true);
        CustomDebug.Log("activate : " + m_stateArray[(int)state].name);
        m_gameState = state;
    }

    public State getCurrentState() {
        return m_gameState;
    }
}
