﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class DerivedNetworManager : NetworkManager
{
    private static DerivedNetworManager m_instance;
    public GameObject[] m_networkObj;
    public static DerivedNetworManager getInstance() {
        return m_instance;
    }

    void Awake() {
        m_instance = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void enableNetworkObj()
    {
        foreach (GameObject obj in m_networkObj)
        {
            obj.SetActive(true);
        }
    }

    void disableNetworkObj()
    {
        foreach (GameObject obj in m_networkObj)
        {
            obj.SetActive(false);
        }
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        CustomDebug.Log("On Client Connect");
        enableNetworkObj();
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        CustomDebug.Log("On Client Disconnect");

        ClanManager.getInstance().onPlayerLeftGame();
        disableNetworkObj();
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        base.OnServerConnect(conn);
        CustomDebug.Log("On Server Connect");

        enableNetworkObj();
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        CustomDebug.Log("On Server Disconnect");
    }

    public override void OnStartHost()
    {
        base.OnStartHost();
        CustomDebug.Log("On Start Host");
        enableNetworkObj();
    }

    public override void OnStopHost()
    {
        base.OnStopHost();
        CustomDebug.Log("On Stop Host");
        ClanManager.getInstance().onPlayerLeftGame();
        ClanManager.getInstance().gameObject.SetActive(false);
        disableNetworkObj();
    }
}