﻿using UnityEngine;
using System.Collections;

public class FollowObject : MonoBehaviour {

    private Transform   m_targetObj     = null;
    public float        m_smoothness    = 10.0f;

    // Use this for initialization
    void Start () {
	
	}

    public void setToFollow(Transform obj) {
        m_targetObj = obj;
    }

	// Update is called once per frame
	void Update () {

        if (m_targetObj == null)
            return;

        Vector3 targetPos = new Vector3(m_targetObj.position.x, m_targetObj.position.y, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, targetPos, Time.deltaTime * m_smoothness);
    }
}
