﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class NetworkDataManager : NetworkBehaviour {

    private static NetworkDataManager m_instance;
    private List<GameObject> m_networkSpawnList = new List<GameObject>();

    public static NetworkDataManager getInstance() {
        return m_instance;
    }

    void Awake() {
        m_instance = this;
    }

    public void addSpawnedPlayer(GameObject obj) {
        m_networkSpawnList.Add(obj);
    }

    public void removeSpawnedPlayer(GameObject obj) {
        m_networkSpawnList.Remove(obj);
    }

    public List<GameObject> getSpawnedPlayerList() {
        return m_networkSpawnList;
    }

    public void removeNetworkObjects() {
        foreach (GameObject obj in m_networkSpawnList)
        {
            Destroy(obj);
        }
    }
}
