﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class BallSpawner : NetworkBehaviour {

    public GameObject m_ballPrefab;
    public Transform[] m_ballPositions;

    private static BallSpawner m_instance;
    public static BallSpawner getInstance()
    {
        return m_instance;
    }

    private void Awake()
    {
        m_instance = this;
    }

    public void spawnBomb()
    {
        if (!isServer)
            return;
        foreach (Transform t in m_ballPositions)
        {
            Vector3 pos = t.position;

            GameObject obj = (GameObject)Instantiate(m_ballPrefab, pos, Quaternion.identity, transform);
            NetworkServer.Spawn(obj);
        }
    }
}
