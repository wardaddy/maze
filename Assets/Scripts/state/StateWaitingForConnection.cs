﻿using UnityEngine;
using System.Collections;

public class StateWaitingForConnection : MonoBehaviour {

	// Use this for initialization
	void Start () {

    }

// Update is called once per frame
    void Update () {
        if (ClanManager.getInstance() != null && ClanManager.getInstance().gameObject.activeSelf)
            StateManager.getInstance().changeState(StateManager.State.STATE_TEAM_SELECTION);
	}
}
