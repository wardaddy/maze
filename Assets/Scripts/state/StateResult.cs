﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StateResult : MonoBehaviour {

    public Text m_ctDeathCount;
    public Text m_tDeathCount;
    public Text m_safehouse;

    private float m_startTime;
    private float m_waitTime = 10.0f;
	// Use this for initialization
	void Start () {
       
    }

    private void OnEnable()
    {
        m_ctDeathCount.text = "CT : " + ClanManager.getInstance().getCtDeathCount();
        m_tDeathCount.text = "T : " + ClanManager.getInstance().getTDeathCount();

        if (GameplayManager.getInstance().m_destroyedSafehouse == GameplayManager.ResultTeam.TEAM_T)
            m_safehouse.text = "Safehouse Destroyed : Terrorist";
        else if (GameplayManager.getInstance().m_destroyedSafehouse == GameplayManager.ResultTeam.TEAM_CT)
            m_safehouse.text = "Safehouse Destroyed : Counter T";
        else
            m_safehouse.text = "Safehouse Destroyed : None";

        m_startTime = Time.time;
    }

	// Update is called once per frame
	void Update () {
        if (Time.time - m_startTime > m_waitTime) {
            m_startTime = 0;
            // game reset
            ClanManager.getInstance().onPlayerLeftGame();
            StateManager.getInstance().changeState(StateManager.State.STATE_TEAM_SELECTION);
        }
	}

    public void onOkClick() {
        // game reset
        ClanManager.getInstance().onPlayerLeftGame();
        StateManager.getInstance().changeState(StateManager.State.STATE_TEAM_SELECTION);
    }
}
