﻿using UnityEngine;
using System.Collections;

public class StateWaitingForPlayer : MonoBehaviour {

    public GameObject m_canvas;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (ClanManager.getInstance().getCtList().Count > 0 &&
            ClanManager.getInstance().getTList().Count > 0)
        {
            BallSpawner.getInstance().spawnBomb();
            StateManager.getInstance().changeState(StateManager.State.STATE_GAMEPLAY);  
        }
    }
}
