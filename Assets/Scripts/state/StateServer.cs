﻿using UnityEngine;
using System.Collections;

public class StateServer : MonoBehaviour {

    public GameObject m_lanHost;
    public GameObject m_lanClient;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onClickLanHost() {
        ClanManager.getInstance().createLANServer();
    }

    public void onClickLanClient() {
        ClanManager.getInstance().joinLANServer();
    }
}
