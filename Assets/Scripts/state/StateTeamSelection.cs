﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StateTeamSelection : MonoBehaviour {

    public GameObject m_buttonCT;
    public GameObject m_buttonT;
    public GameObject m_nameField;

	// Use this for initialization
	void Start () {
	
	}

    private void OnEnable()
    {
        m_buttonCT.SetActive(false);
        m_buttonT.SetActive(false);
        m_nameField.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void onClickCT() {
        ClanManager.getInstance().spawnCT();
    }

    public void onClickT() {
        ClanManager.getInstance().spawnT();
    }

    public void onClickOk()
    {
        ClanManager.getInstance().setLocalPlayerName(m_nameField.transform.FindChild("name").GetComponent<Text>().text);
        m_buttonCT.SetActive(true);
        m_buttonT.SetActive(true);
        m_nameField.SetActive(false);
    }
}
