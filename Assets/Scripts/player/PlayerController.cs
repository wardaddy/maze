﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// Class Controls player actions and player properties
/// </summary>
public class PlayerController : NetworkBehaviour {

    /// <summary>
    /// Enum denotes player's team
    /// </summary>
    public enum Team {
        NONE = 0,
        COUNTER_TERRORIST,
        TERRORIST,
    };

    public enum State {
        STATE_NONE,
        STATE_ALIVE,
        STATE_DEAD,
    };

    [SyncVar(hook = "onUpdatedBombId")]
    private NetworkInstanceId m_bombInstanceId;
    public GameObject m_collectedBomb = null;

    public GameObject m_bulletPrefab;
    public Transform m_bulletSpawn;
    public Camera m_camera;


    [SyncVar(hook = "OnChangedDeathCount")]
    private int m_deathCount = 0;

    /// <summary>
    /// m_teamId is sync var and on change, onSetPlayerTeam will get called on all clients
    /// </summary>
    [SyncVar(hook = "onSetPlayerTeam")]
    public Team m_teamId = Team.NONE;

    [SyncVar(hook = "OnSetPlayerName")]
    private string m_playerName;

    public Text m_playerNameObj;
    private State m_playerState = State.STATE_NONE;

    public LineRenderer m_lineRenderer;

    private void Awake()
    {
        LayerCache.Awake(gameObject);
    }

    private void OnDestroy()
    {
        LayerCache.OnDestroy(gameObject);
    }
    // Use this for initialization
    /// <summary>
    /// if local player then setting camera to follow local player.
    /// </summary>
    void Start() {
        if (isLocalPlayer)
        {
            m_camera = FindObjectOfType<Camera>();
            m_camera.GetComponent<FollowObject>().setToFollow(transform);
        }
    }

    // Update is called once per frame
    /// <summary>
    /// updates each frame for local player only
    /// </summary>
    /// <remarks>
    /// upates player position and rotation on navigational key press
    /// Fires bullet on pressing SPACE button
    /// </remarks>
    void Update() {
        // Render radar
        GameObject localPlayer = ClanManager.getInstance().getLocalPlayer();
        if (m_collectedBomb != null && localPlayer != null && localPlayer.GetComponent<PlayerController>().m_teamId == m_teamId)
            updateBombRadar();

        if (!isLocalPlayer)
            return;

        if (m_playerState != State.STATE_ALIVE || StateManager.getInstance().getCurrentState() != StateManager.State.STATE_GAMEPLAY)
            return;

        Vector3 moveDirection = Vector3.zero;
        if (isNavigationKeyDown())
        {
            CharacterController cc = GetComponent<CharacterController>();
            Vector3 pos = transform.position;

            Vector3 targetPos = transform.position + (new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0.0f) * 5.0f);


            moveDirection = targetPos - transform.position;
            moveDirection *= Time.deltaTime;

            cc.Move(moveDirection);

            if (moveDirection != Vector3.zero)
            {
                Vector3 actualMoveDirection = Vector3.Cross(moveDirection, transform.up);
                transform.rotation = Quaternion.LookRotation(actualMoveDirection, transform.up);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && !hasBomb())
        {
            CmdFire();
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            if (hasBomb())
                Cmd_dropBomb();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (hasBomb())
                Cmd_plantedBomb();
            else
            {
                // find nearest bomb and diffuse
                GameObject nearestBomb = getNearestBomb();
                if (nearestBomb != null)
                {
                    Cmd_bombDiffused(nearestBomb.GetComponent<NetworkIdentity>().netId);
                }
                else
                {
                    CustomDebug.Log("No Bomb To Diffuse.. Chu**** banaya");
                }
            }
        }
    }

    GameObject getNearestBomb()
    {
        List<GameObject> bombCache = LayerCache.getObjectListInLayer(LayerMask.NameToLayer("bomb"));
        GameObject nearestBomb = null;
        float minDist = 1.0f;

        if (bombCache != null)
        {
            foreach (GameObject obj in bombCache)
            {
                Bomb bomb = obj.GetComponent<Bomb>();
                if (bomb != null && bomb.getState() == Bomb.State.BOMB_PLANTED)
                {
                    float bombDist = Vector3.Distance(transform.position, obj.transform.position);
                    CustomDebug.Log("Bomb Distance : " + bombDist);
                    if (bombDist < minDist)
                    {
                        nearestBomb = obj;
                        minDist = bombDist;
                    }
                }
            }
        }

        return nearestBomb;
    }
    /// <summary>
    /// checks if navigation key is down
    /// </summary>
    /// <returns>TRUE if navigation key is down</returns>
    bool isNavigationKeyDown() {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow)
            || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
            return true;
        return false;
    }

    /// <summary>
    /// This command function and commands server to instantiate bullet on all other clients
    /// Also this function asks host to set bullet direction and parent. parent object in bullet is sync var.
    /// </summary>
    [Command]
    void CmdFire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            m_bulletPrefab,
            m_bulletSpawn.position,
            m_bulletSpawn.rotation);

        //NetworkServer.Spawn(bullet);
        bullet.GetComponent<Bullet>().setParameters(gameObject.GetComponent<NetworkIdentity>().netId);
        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
        Rpc_fire();
    }

    [ClientRpc]
    void Rpc_fire()
    {
        // Create the Bullet from the Bullet Prefab
        var bullet = (GameObject)Instantiate(
            m_bulletPrefab,
            m_bulletSpawn.position,
            m_bulletSpawn.rotation);

        //NetworkServer.Spawn(bullet);
        bullet.GetComponent<Bullet>().setParameters(gameObject.GetComponent<NetworkIdentity>().netId);
        // Destroy the bullet after 2 seconds
        Destroy(bullet, 2.0f);
    }

    /// <summary>
    /// this function gets called when local player is instantiated
    /// </summary>
    /// <remarks> 
    /// this function sets local player's layer to LOCAL
    /// sets player's color based on team id
    /// notifies server to update the properties on other clients also
    /// sets player name</remarks>
    public override void OnStartLocalPlayer() {
        LayerCache.changeLayerTo(gameObject, LayerMask.NameToLayer("local"));

        Color color = ClanManager.getInstance().m_shouldSpawnTerrorist ? Color.red : Color.blue;
        GetComponent<MeshRenderer>().material.color = color;

        m_teamId = ClanManager.getInstance().m_shouldSpawnTerrorist ? Team.TERRORIST : Team.COUNTER_TERRORIST;
        CmdSetPlayerTeam(m_teamId);
        Cmd_setPlayerName(ClanManager.getInstance().getLocalPlayerName());
        ClanManager.getInstance().setLocalPlayer(gameObject);
        CustomDebug.Log("set Local Player : " + gameObject.name);
    }

    /// <summary>
    /// command for server to set player properties
    /// </summary>
    /// <param name="teamId"> TEAM id </param>
    [Command]
    void CmdSetPlayerTeam(Team teamId) {
        if (!isServer)
            return;
        
        CustomDebug.Log("Rpc Team Id : " + m_teamId);
        Color teamColor = Color.white;

        m_teamId = teamId;

        if (m_teamId == Team.TERRORIST)
        {
            ClanManager.getInstance().terroristHasSpawned(gameObject);
            if(ClanManager.getInstance().m_TSpawnPoints.Count > 0)
                transform.position = ClanManager.getInstance().m_TSpawnPoints[0].position;
            teamColor = Color.red;
        }
        else
        {
            ClanManager.getInstance().counterTHasSpawned(gameObject);
            if (ClanManager.getInstance().m_CTSpawnPoints.Count > 0)
                transform.position = ClanManager.getInstance().m_CTSpawnPoints[0].position;
            teamColor = Color.blue;
        }

        m_playerNameObj.text = m_playerName = ClanManager.getInstance().getLocalPlayerName();        
        GetComponent<MeshRenderer>().material.color = teamColor;
        m_playerState = State.STATE_ALIVE;
    }

    [Command]
    void Cmd_setPlayerName(string name)
    {
        if (!isServer)
            return;

        m_playerNameObj.text = m_playerName = name;
    }

    void OnSetPlayerName(string name)
    {
        if (isServer)
            return;
        m_playerNameObj.text = m_playerName = name;
    }

    /// <summary>
    /// This is hook function of var m_teamId.
    /// </summary>
    /// <param name="teamId"></param>
    void onSetPlayerTeam(Team teamId) {

        if (isServer)
            return;
        
        Color teamColor = Color.white;

        m_teamId = teamId;

        CustomDebug.Log("Sync Team Id : " + m_teamId);

        if (m_teamId == Team.TERRORIST)
        {
            ClanManager.getInstance().terroristHasSpawned(gameObject);
            teamColor = Color.red;
        }
        else
        {
            ClanManager.getInstance().counterTHasSpawned(gameObject);
            teamColor = Color.blue;
        }

        GetComponent<MeshRenderer>().material.color = teamColor;
        m_playerState = State.STATE_ALIVE;
    }

    /// <summary>
    /// called on client start
    /// </summary>
    public override void OnStartClient()
    {
        base.OnStartClient();

        CustomDebug.Log("Spawned Network player : " + gameObject.name);
        PlayerController pController = gameObject.GetComponent<PlayerController>();
        if (pController != null) {
            GetComponent<MeshRenderer>().material.color = pController.m_teamId == Team.TERRORIST ? Color.red : Color.blue;
        }

        gameObject.layer = LayerMask.NameToLayer("player");
        NetworkDataManager.getInstance().addSpawnedPlayer(gameObject);

        if (!isLocalPlayer && ClanManager.getInstance().getLocalPlayer() == null)
        {
            CustomDebug.Log("already spawned Network player : " + m_teamId);
            if (m_teamId == Team.TERRORIST)
                ClanManager.getInstance().addT(gameObject);
            else if (m_teamId == Team.COUNTER_TERRORIST)
                ClanManager.getInstance().addCt(gameObject);
        }
    }

    /// <summary>
    /// gets called on destroying network player
    /// </summary>
    public override void OnNetworkDestroy()
    {
        base.OnNetworkDestroy();

        CustomDebug.Log("Destroyed Network player : " + gameObject.name);
        if (hasBomb() && m_collectedBomb.GetComponent<Bomb>().getState() == Bomb.State.BOMB_PICKED)
        {
            Cmd_dropBomb();
        }
            
        NetworkDataManager.getInstance().removeSpawnedPlayer(gameObject);

        if (m_teamId == Team.TERRORIST)
            ClanManager.getInstance().terroristHasDestroyed(gameObject);
        else
            ClanManager.getInstance().counterTHasDestroyed(gameObject);
    }

    [Command]
    public void Cmd_collectedBomb(NetworkInstanceId objId)
    {
        if (!isServer)
            return;
        CustomDebug.Log("Client RPC Collected Bomb : " + objId);
        
        m_bombInstanceId = objId;
        m_collectedBomb = NetworkServer.FindLocalObject(objId);

        //m_collectedBomb.SetActive(false);
    }

    void onUpdatedBombId(NetworkInstanceId objId)
    {
        if (objId != NetworkInstanceId.Invalid)
        {
            CustomDebug.Log("Hook On Collected Bomb " + objId);
            m_bombInstanceId = objId;
            m_collectedBomb = ClientScene.FindLocalObject(objId);
//            m_collectedBomb = NetworkServer.FindLocalObject(objId);
            CustomDebug.Log("Hook On Collected Bomb Null : " + (m_collectedBomb == null));
            if (m_collectedBomb != null)
            {
                //m_collectedBomb.SetActive(false);
                m_collectedBomb.GetComponent<Bomb>().onBombPicked(this);
                m_lineRenderer.gameObject.SetActive(true);
            }
        }
        else
        {
            CustomDebug.Log("network instance id is invalid");
            m_collectedBomb.transform.position = transform.position - transform.right * 1.0f;
            //m_collectedBomb.SetActive(true);
            m_collectedBomb = null;
            m_lineRenderer.gameObject.SetActive(false);
        }
    }

    bool hasBomb()
    {
        return (m_collectedBomb != null);
    }

    [Command]
    void Cmd_dropBomb()
    {
        CustomDebug.Log("COMMAND DROP Bomb");
        // update bomb status on client
        m_collectedBomb.GetComponent<Bomb>().onBombDropped();
        // inform all others about state change
        m_bombInstanceId = NetworkInstanceId.Invalid;
    }

    [Command]
    void Cmd_plantedBomb()
    {
        CustomDebug.Log("Planted bomb");
        m_collectedBomb.GetComponent<Bomb>().onBombPlanted();
        m_bombInstanceId = NetworkInstanceId.Invalid;
    }

    [Command]
    void Cmd_bombDiffused(NetworkInstanceId bombId)
    {
        CustomDebug.Log("Bomb Diffused");
        GameObject bomb = NetworkServer.FindLocalObject(bombId);
        bomb.GetComponent<Bomb>().onBombDiffused();
    }

    public void onDeath()
    {
        CustomDebug.Log("On Death");
        changeState(PlayerController.State.STATE_DEAD);
        m_deathCount += 1;
    }

    void OnChangedDeathCount(int deathCount)
    {
        changeState(PlayerController.State.STATE_DEAD);
        m_deathCount = deathCount;
    }

    public int getDeathCount() {
        return m_deathCount;
    }

    public void changeState(State state)
    {
        m_playerState = state;
        CustomDebug.Log("Player state changed to : " + state);
        switch (m_playerState)
        {
            case State.STATE_ALIVE:
                onStateAlive();
                break;
            case State.STATE_DEAD:
                onStateDead();
                break;
        }
    }

    private void onStateAlive() {
        GetComponent<MeshRenderer>().material.color = (m_teamId == Team.COUNTER_TERRORIST) ? Color.blue : Color.red;
    }

    private void onStateDead()
    {
        GetComponent<MeshRenderer>().material.color = Color.black;
    }

    private void updateBombRadar()
    {
        float theta = 0;
        float thetaScale = 0.01f;
        int size = (int)(1.0f / thetaScale) + 1;
        float blastRadius = m_collectedBomb.GetComponent<Bomb>().m_blastRadius;

        m_lineRenderer.SetVertexCount(size);
        m_lineRenderer.SetWidth(0.1f, 0.1f);
        for (int i = 0; i < size; i++)
        {
            theta += (2.0f * Mathf.PI * thetaScale);
            float x = transform.position.x + blastRadius * Mathf.Cos(theta);
            float y = transform.position.y + blastRadius * Mathf.Sin(theta);
            m_lineRenderer.SetPosition(i, new Vector3(x, y, 0));
        }
    }
}
