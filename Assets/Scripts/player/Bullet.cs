﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

/// <summary>
/// This class handles bullet functionality
/// </summary>
public class Bullet : NetworkBehaviour {

    public float        speed           = 0.1f;

    /// <summary>
    /// m_parent is sync var and hook function is onParentSet
    /// </summary>
    private GameObject   m_parent = null;

    private NetworkInstanceId m_parentNetId = NetworkInstanceId.Invalid;

    private void Awake()
    {
        LayerCache.Awake(gameObject);
    }

    private void OnDestroy()
    {
        LayerCache.OnDestroy(gameObject);
    }
    // Use this for initialization
    void Start () {
 
    }

    /// <summary>
    /// This is RPC function and executes on server only
    /// </summary>
    /// <param name="dir"> Bullet direction</param>
    /// <param name="parent"> Parent object</param>
    public void setParameters(NetworkInstanceId parentId) {
        
        CustomDebug.Log("COMMAND SET BULLET PARENT : " + parentId + "isServer : "+isServer);
        m_parentNetId = parentId;
        if (isServer)
            m_parent = NetworkServer.FindLocalObject(m_parentNetId);
        else
            m_parent = ClientScene.FindLocalObject(m_parentNetId);
    }

	// Update is called once per frame
	void Update () {
        if (m_parent == null)
            return;
        transform.position += transform.right * speed;
	}

    /// <summary>
    /// Callback when two objects collide
    /// if collided objects are of different layer and different team id then it registers hit on impacted object
    /// </summary>
    /// <param name="collider"></param>
    void OnTriggerEnter(Collider collider) {
        var hit = collider.gameObject;

        if (hit == m_parent || hit.layer == LayerMask.NameToLayer("bullet") || hit.layer == LayerMask.NameToLayer("wall"))
            return;

        CustomDebug.Log("Hit layer : " + hit.layer + "    hit name : "+hit.name);
        CustomDebug.Log("Nameto Layer : " + LayerMask.NameToLayer("player"));

        CustomDebug.Log("Parent : " + (m_parent == null));
        CustomDebug.Log("Hit : " + (hit == null));
        

        var playerController = hit.GetComponent<PlayerController>();
        var parentController = m_parent.GetComponent<PlayerController>();

        
        CustomDebug.Log("PlayerController : " + (playerController == null));
        CustomDebug.Log("ParentController : " + (parentController == null));
        
        if (playerController != null && parentController != null)
        {
            CustomDebug.Log("Playercontroller isTerrorist : " + playerController.m_teamId);
            CustomDebug.Log("Playercontroller isTerrorist : " + parentController.m_teamId);

            if (playerController.m_teamId == parentController.m_teamId) //same team
                return;
        }

        var health = hit.GetComponent<Health>();

        if (health != null && gameObject.layer != hit.layer) {
            CustomDebug.Log("Hit : Health down by 10");            
            health.TakeDamage(10);
            Destroy(gameObject);
        }
    }
}
