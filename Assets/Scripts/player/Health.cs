﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

/// <summary>
/// This class manages player's health
/// </summary>
public class Health : NetworkBehaviour {

    public const int    m_maxHealth         = 100;
    [SyncVar(hook = "OnChangeHealth")]
    public int          m_currentHealth     = m_maxHealth;
    public bool         m_destroyOnDeath    = false;

    private NetworkStartPosition[]      m_spawnPoints;
    public RectTransform                m_healthBar;

    void Start()
    {
        if (isLocalPlayer)
        {
            m_spawnPoints = FindObjectsOfType<NetworkStartPosition>();
            CustomDebug.Log("Spawn Length : " + m_spawnPoints.Length);
        }
    }

    /// <summary>
    /// TakeDamage reduces health by amount.
    /// Notifies server to update health on all clients
    /// if player dies, then ask server to respawn it on each client
    /// </summary>
    /// <param name="amount"></param>
    public void TakeDamage(int amount)
    {

            CustomDebug.Log("Reduced helth");

        m_currentHealth -= amount;
        if (isServer)
            Rpc_UpdateHealth(m_currentHealth);
        //else
        //    Cmd_UpdateHealth(m_currentHealth);

        if (m_currentHealth <= 0)
        {
            m_currentHealth = 0;
            if (m_destroyOnDeath)
                Destroy(gameObject);

            if (isServer)
            {
                GetComponent<PlayerController>().onDeath();
                if(!m_destroyOnDeath)
                    RpcRespawn();
            }
        }
    }

    public void instantDeath() {
        //Destroy(gameObject);
        CustomDebug.Log("Instant Death : " + gameObject.name);
        GetComponent<PlayerController>().onDeath();
    }

    /// <summary>
    /// Server call to update health of player on all client
    /// </summary>
    /// <param name="health"></param>
    [ClientRpc]
    void Rpc_UpdateHealth(int health) {

            CustomDebug.Log("Update health on Client");

        m_currentHealth = health;
    }

    /// <summary>
    /// client asks server to update health value
    /// </summary>
    /// <param name="health"></param>
    [Command]
    void Cmd_UpdateHealth(int health)
    {

        CustomDebug.Log("Update health on server");

        m_currentHealth = health;
    }

    /// <summary>
    /// Hook function to update health. Hook is attached to m_currentHealth
    /// </summary>
    /// <param name="health"></param>
    void OnChangeHealth(int health)
    {

            CustomDebug.Log("on change health : " + health);

        m_healthBar.sizeDelta = new Vector2(health, m_healthBar.sizeDelta.y);
    }

    /// <summary>
    /// Executes by server and respawns player. Also commands to server to update health of player
    /// </summary>
    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            // Set the spawn point to origin as a default value
            Vector3 spawnPoint = Vector3.zero;

            // If there is a spawn point array and the array is not empty, pick a spawn point at random
            if (GetComponent<PlayerController>().m_teamId == PlayerController.Team.COUNTER_TERRORIST && ClanManager.getInstance().m_CTSpawnPoints.Count > 0)
            {
                spawnPoint = ClanManager.getInstance().m_CTSpawnPoints[0].position;
            }
            else if (GetComponent<PlayerController>().m_teamId == PlayerController.Team.COUNTER_TERRORIST && ClanManager.getInstance().m_CTSpawnPoints.Count > 0)
            {
                spawnPoint = ClanManager.getInstance().m_TSpawnPoints[0].position;
            }
            else if (m_spawnPoints != null && m_spawnPoints.Length > 0)
            {
                spawnPoint = m_spawnPoints[Random.Range(0, m_spawnPoints.Length)].transform.position;
            }

            CustomDebug.Log("Spawn Position : " + spawnPoint);
            // Set the player’s position to the chosen spawn point
            transform.position = spawnPoint;
            CmdRpcHasRespawned();
        }
    }

    [Command]
    void CmdRpcHasRespawned()
    {
        CustomDebug.Log("after respawn current health : " + m_currentHealth);
        m_currentHealth = m_maxHealth;
    }
}
