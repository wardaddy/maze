﻿using UnityEngine;
using System.Collections;

public class Safehouse : MonoBehaviour {

    public bool m_TSafehouse = false;

    private void Awake()
    {
        LayerCache.Awake(gameObject);
    }

    private void OnDestroy()
    {
        LayerCache.OnDestroy(gameObject);
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onDestroyed() {
        if (m_TSafehouse)
            GameplayManager.getInstance().setSafeDestroyedOf(GameplayManager.ResultTeam.TEAM_CT);
        else
            GameplayManager.getInstance().setSafeDestroyedOf(GameplayManager.ResultTeam.TEAM_T);

        GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public void resetSafehouse()
    {
        GetComponent<MeshRenderer>().material.color = Color.white;
    }
}
