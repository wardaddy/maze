﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class Bomb : NetworkBehaviour {

    public enum State {
        BOMB_NONE,
        BOMB_PICKED,
        BOMB_DROPPED,
        BOMB_PLANTED,
        BOMB_DIFFUSED,
        BOMP_EXPLODED,
    }
    
    [SyncVar (hook = "onUpdateState")]
    private State m_state = State.BOMB_NONE;
    private PlayerController m_parent = null;

    public int m_clockTime = 5;
    private float m_clockStartTime = 0;
    public float m_blastRadius = 10.0f;

    public LineRenderer m_lineRenderer = null;
    public float m_thetaScale = 0.01f;

    [SyncVar (hook = "OnUpdateBombTimer")]
    private float m_remainingPercentTime = 100;
    public RectTransform m_timerBar;
    public GameObject m_canvas;

    private void Awake()
    {
        LayerCache.Awake(gameObject);
    }

    private void OnDestroy()
    {
        LayerCache.OnDestroy(gameObject);
    }
    private void Start()
    {
        if (isGameplayType1())
        {
           onBombPlanted();
        }
    }

    private void Update()
    {
        switch (m_state)
        {
            case State.BOMB_PLANTED:
            case State.BOMB_PICKED:
            case State.BOMB_DROPPED:
                updateClock();
            break;
        }

        // Render Radar
        float theta = 0;
        int size = (int) (1.0f / m_thetaScale) + 1;
        m_lineRenderer.SetVertexCount(size);
        m_lineRenderer.SetWidth(0.1f, 0.1f);
        for (int i = 0; i < size; i++) {
            theta += (2.0f * Mathf.PI * m_thetaScale);
            float x = transform.position.x + m_blastRadius * Mathf.Cos(theta);
            float y = transform.position.y + m_blastRadius * Mathf.Sin(theta);
            m_lineRenderer.SetPosition(i, new Vector3(x, y, 0));
        }
    }

    private void updateClock()
    {
        if (isGameplayType1())
        {
            //if (Time.deltaTime * 1000 % 500 == 0)
            {
                if (isServer)
                {
                    m_remainingPercentTime = 100 - ((Time.time - m_clockStartTime) * 100.0f / m_clockTime);
                    m_timerBar.sizeDelta = new Vector2(m_remainingPercentTime, m_timerBar.sizeDelta.y);
                    //CustomDebug.Log("Remaining Time : " + m_remainingPercentTime);
                }
            }
        }

        if (m_remainingPercentTime <= 0.0f)
        {
            onTimerFinished();
        }
    }

    public void OnUpdateBombTimer(float percent) {
        m_remainingPercentTime = percent;
        m_timerBar.sizeDelta = new Vector2(m_remainingPercentTime, m_timerBar.sizeDelta.y);
        //CustomDebug.Log("Remaining Time : " + m_remainingPercentTime);
    }

    private void resetClock()
    {
        m_clockTime = 0;
        m_clockStartTime = 0;
        m_state = State.BOMB_NONE;
    }

    public void setState(State state)
    {
        m_state = state;

        switch (m_state)
        {
            /*case State.BOMB_PICKED:
                // Player will set itself as parameter
                break;*/
            case State.BOMB_DROPPED:
                onBombDropped();
                break;
            case State.BOMB_PLANTED:
                onBombPlanted();
                break;
            case State.BOMB_DIFFUSED:
                onBombDiffused();
                break;
            //case State.BOMP_EXPLODED:
            //    onBombExploded();
            //    break;
        }
    }

    public State getState()
    {
        return m_state;
    }

    void onUpdateState(State state)
    {
        CustomDebug.Log("Bomb state changed on server to : " + state);
        setState(state);
    }

    public void onBombPicked(PlayerController parent)
    {
        CustomDebug.Log("On Bomb Picked By : " + parent.name);
        m_parent = parent;
        m_state = State.BOMB_PICKED;
        m_canvas.SetActive(false);
        m_lineRenderer.gameObject.SetActive(false);
        GetComponent<MeshRenderer>().enabled = false;
    }

    public void onBombDropped()
    {
        CustomDebug.Log("On Bomb Dropped");
        m_parent = null;
        m_state = State.BOMB_DROPPED;
        m_canvas.SetActive(true);
        m_lineRenderer.gameObject.SetActive(true);
        GetComponent<MeshRenderer>().enabled = true;
    }

    public void onBombPlanted()
    {
        CustomDebug.Log("On Bomb Planted");
        m_state = State.BOMB_PLANTED;
        GetComponent<MeshRenderer>().material.color = Color.red;
        m_clockStartTime = Time.time;
    }

    public void onBombDiffused()
    {
        CustomDebug.Log("On Bomb Diffused");
        m_state = State.BOMB_DIFFUSED;
        GetComponent<MeshRenderer>().material.color = Color.gray;
    }

    public void onBombExploded()
    {
        CustomDebug.Log("On Bomb Exploded");
        GetComponent<MeshRenderer>().material.color = Color.white;

        if (isServer)
        {
            CustomDebug.Log("server Destroy nearby objects");
            destroyNearbyObjects();
            Rpc_onBombExploded();
            resetClock();
            StateManager.getInstance().changeState(StateManager.State.STATE_RESULT);
        }
    }

    [ClientRpc]
    public void Rpc_onBombExploded()
    {
        if (isServer)
            return;

        CustomDebug.Log("RPC On Bomb Exploded");
        GetComponent<MeshRenderer>().material.color = Color.white;
        destroyNearbyObjects();
        resetClock();
        StateManager.getInstance().changeState(StateManager.State.STATE_RESULT);
    }

    private void onTimerFinished()
    {
        if (isServer)
        {
            CustomDebug.Log("On Bomb Timer Finished");
            // TODO: run explosion animation
            m_state = State.BOMP_EXPLODED;
            onBombExploded();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_state == State.BOMB_DIFFUSED || m_state == State.BOMP_EXPLODED || (m_state == State.BOMB_PLANTED && !isGameplayType1()))
            return;

        CustomDebug.Log("Ball OnTriggerEnter");
        var hit = other.gameObject;

        PlayerController player = hit.GetComponent<PlayerController>();

        if(player != null && isServer)
        {
            CustomDebug.Log("Call Rpc CollectedBall");
            // ball collected
            player.Cmd_collectedBomb(gameObject.GetComponent<NetworkIdentity>().netId);
        }
    }

    private bool isGameplayType1() {
        return (GameplayManager.getInstance().m_gameplayType == GameplayManager.GameplayType.TYPE_1);
    }

    void destroyNearbyObjects() {
        CustomDebug.Log("Destroy Nearby Objects");
        List<GameObject> nearbyObj = getObjectsNearby();

        foreach (GameObject obj in nearbyObj) {
            CustomDebug.Log("Nearby obj : " + obj.name);
            if (obj.layer == LayerMask.NameToLayer("local") || obj.layer == LayerMask.NameToLayer("player"))
            {
                CustomDebug.Log("destroyed obj : " + obj.name);
                obj.GetComponent<Health>().instantDeath();
            }
            else if (obj.layer == LayerMask.NameToLayer("safehouse")) {
                CustomDebug.Log("destroyed obj : " + obj.name);
                obj.GetComponent<Safehouse>().onDestroyed();
            }
        }
    }

    List<GameObject> getObjectsNearby() {

        List<GameObject> nearbyObjList = new List<GameObject>();

        List<GameObject> playerCache = LayerCache.getObjectListInLayer(LayerMask.NameToLayer("player"));

        foreach (GameObject obj in playerCache) {
            if (Vector3.Distance(obj.transform.position, transform.position) < m_blastRadius)
                nearbyObjList.Add(obj);
        }

        List<GameObject> localPlayerCache = LayerCache.getObjectListInLayer(LayerMask.NameToLayer("local"));
        foreach (GameObject obj in localPlayerCache)
        {
            if (Vector3.Distance(obj.transform.position, transform.position) < m_blastRadius)
                nearbyObjList.Add(obj);
        }

        List<GameObject> safeHouseCache = LayerCache.getObjectListInLayer(LayerMask.NameToLayer("safehouse"));
        if (safeHouseCache != null)
        {
            foreach (GameObject obj in safeHouseCache)
            {
                if (Vector3.Distance(obj.transform.position, transform.position) < m_blastRadius)
                    nearbyObjList.Add(obj);
            }
        }

        return nearbyObjList;
    }

    public override void OnStartClient()
    {
        NetworkDataManager.getInstance().addSpawnedPlayer(gameObject);
    }
}
